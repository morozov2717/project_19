const express = require('express')
const morgan = require('morgan')
const Joi = require('joi')
const path = require('path')
var bodyParser = require('body-parser')

const app = express()

app.set('view engine', 'ejs')

const PORT = process.env.PORT || 3000
let films = [
{genre:'drama', name: "Escape from Shawshank", description:"Accountant Andy Dufresne is accused of murdering his own wife and her lover. Once in a prison called Shawshank, he is confronted with cruelty and lawlessness reigning on both sides of the bars. Everyone who gets into these walls becomes their slave for the rest of their lives. But Andy, who has a lively mind and a kind soul, finds an approach to both prisoners and guards, seeking their special favor."},
{genre:'fantasy', name: "The Green Mile", description:"Paul Edgecombe is the head of the death row unit at the Cold Mountain prison, each of whose prisoners once passes the 'green mile' on the way to the place of execution. Paul has seen a lot of prisoners and guards during his work. However, the giant John Coffey, accused of a terrible crime, became one of the most unusual inhabitants of the block."},
{genre:'drama', name: "Schindler 's List", description:"The film tells the real story of the mysterious Oskar Schindler, a member of the Nazi Party, a successful manufacturer who saved almost 1,200 Jews during World War II."}
]

app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

app.get('/', (req, res)=>{
    res.render(path.resolve(__dirname, 'views', 'home.ejs'))
})

app.get('/films', (req, res)=>{
    res.render(path.resolve(__dirname, 'views', 'films.ejs'), { films })
})

app.get('/genre', (req, res)=>{
    res.render(path.resolve(__dirname, 'views', 'genre.ejs'))
})

app.get('/genre/:id', (req, res)=>{
    let genre = [] 
    films.forEach(element => {if(element.genre == req.params.id) genre.push(element)})
    switch(req.params.id){
        case "drama":{
            res.render(path.resolve(__dirname, 'views', 'drama.ejs'), { genre })
            break
        }
        case "fantasy":{
            res.render(path.resolve(__dirname, 'views', 'fantasy.ejs'), { genre })
            break
        }
        default:{
            res.send("There is no film of this genre")
        }
    }
})

app.get('/films/:id/uniquecode', midl, (req, res)=>{
    res.send("There's nothing here")
})

app.post('/films', (req, res)=>{
    const schema = {
        name: Joi.string().min(3).required(),
        genre: Joi.string().min(3).required(),
        description: Joi.string().required()
    }
    const result = Joi.validate(req.body, schema)
    if(result.error){
        return res.status(500).send(result.error.datails[0].message)
    }
    console.log(req.body.description)
    const film = {
    genre: req.body.genre, name: req.body.name, description: req.body.description
    }
    films.push(film)
    res.send(film)
})

app.listen(PORT, ()=>{console.log(`the server is running on port ${PORT}...`)})

function midl(req, res, next){
    if(req.query.admin === 'true'){
    let film = []
    films.forEach(element => {if(element.genre == req.params.id) film.push(element)})
    if(film.length === 0) return res.status(404).send('there is no film of this genre')
    res.send('123456789')
    }else{
       next()
    }
}

app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});